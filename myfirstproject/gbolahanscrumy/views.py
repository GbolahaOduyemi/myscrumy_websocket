from django.http import Http404, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404, redirect
from django.db import models
from django.contrib.auth.models import User, Group
from gbolahanscrumy.models import *
from django.http import HttpResponse
from django.conf.urls import url
from django.contrib import admin, messages
from django.contrib.auth import views as auth_views
from django.contrib.auth.forms import authenticate
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import  get_user_model, login, authenticate
from gbolahanscrumy.forms import (DeveloperCreateGoalForm, QACreateGoalForm, SignUpForm,
                                        QAChangeGoalForm1, ChangeGoalForm, OwnerChangeGoalForm, OwnerCreateGoalForm,
                                        QAChangeGoalForm, AdminChangeGoalForm,DeveloperChangeGoalForm)
import random

def index1(request):
    return HttpResponse("This is a Scrum Application")

User = get_user_model()
# def index(request, *args, **kwargs):    
  #  qs = ScrumyGoals.objects.filter(goal_name="Learn Django").get()
   #  return HttpResponse(f'{qs}')
@login_required(login_url="/gbolahanscrumy/accounts/login")
def home(request):
    current_user = request.user
    users = User.objects.all()
    def get_by_status(status_name):
        goals = GoalStatus.objects.get(status_name=status_name)
        status_goals = goals.scrumygoals_set.all()
        return status_goals

    daily_goals = get_by_status("Daily Goal")
    weekly_goals = get_by_status("Weekly Goal")
    verify_goals = get_by_status("Verify Goal")
    done_goals = get_by_status("Done Goal")

    dictionary = {
        'users': users,
        'weekly_goals': weekly_goals,
        'daily_goals': daily_goals,
        'verify_goals': verify_goals,
        'done_goals': done_goals,
    }
    return render(request, 'gbolahanscrumy/home.html', dictionary)

def move_goal(request, goal_id):
    current_user = request.user
    usr_grp = request.user.groups.all()[0]
    print(usr_grp)
    goals = get_object_or_404(ScrumyGoals, goal_id=goal_id)

    if usr_grp == Group.objects.get(name='Developer') and current_user == goals.user:
    	if request.method == 'POST':
            form = DeveloperChangeGoalForm(request.POST)
            if form.is_valid():
                selected_status = form.save(commit=False)
                selected = GoalStatus.objects.all()
                selected = form.cleaned_data['goal_status']
                choice = GoalStatus.objects.get(id=int(selected))
                goals.goal_status = choice
                goals.save()
                return redirect("/gbolahanscrumy/home")
    		
    	else:
            form = DeveloperChangeGoalForm()
        

    elif usr_grp == Group.objects.get(name='Quality Assurance') and current_user == goals.user:
        if request.method == 'POST':
            form = QAChangeGoalForm(request.POST)
            if form.is_valid():
                selected_status = form.save(commit=False)
                selected = GoalStatus.objects.all()
                selected = form.cleaned_data['goal_status']
                # get_status = selected_status.goal_status
                choice = GoalStatus.objects.get(id=int(selected))
                goals.goal_status = choice
                goals.save()
                return redirect ("/gbolahanscrumy/home")
        else:
            form = QAChangeGoalForm()

    elif usr_grp == Group.objects.get(name='Quality Assurance') and current_user != goals.user:
        print(current_user != goals.user)
        if request.method == 'POST':
            form = QAChangeGoalForm1(request.POST)
            if form.is_valid():
                selected_status = form.save(commit=False)
                selected = GoalStatus.objects.all()
                selected = form.cleaned_data['goal_status']
                choice = GoalStatus.objects.get(id=int(selected))
                goals.goal_status = choice
                goals.save()
                return redirect ("/gbolahanscrumy/home")
        form = QAChangeGoalForm1()

    elif usr_grp == Group.objects.get(name='Owner') and current_user == goals.user:
        if request.method == 'POST':
            form = OwnerChangeGoalForm(request.POST)
            if form.is_valid():
                selected_status = form.save(commit=False)
                selected = form.cleaned_data['goal_status']
                choice = GoalStatus.objects.get(id=int(selected))
                goals.goal_status = choice
                goals.save()
                return redirect ("/gbolahanscrumy/home")
        form = OwnerChangeGoalForm()

    elif usr_grp == Group.objects.get(name='Admin'):
        if request.method == 'POST':
            form = AdminChangeGoalForm(request.POST)
            if form.is_valid():
                selected_status = form.save(commit=False)
                selected = form.cleaned_data['goal_status']
                choice = GoalStatus.objects.get(id=int(selected))
                goals.goal_status = choice
                goals.save()
                return redirect ("/gbolahanscrumy/home")
        
    else:
            
        return HttpResponse('You  are not authorised to move this goal')
    form = AdminChangeGoalForm()
    return render(request, 'gbolahanscrumy/movegoal.html',
                  {'form': form, 'goals': goals, 'current_user': current_user})

  #  obj = ScrumyGoals.objects.get(goal_id=goal_id)
   # form = MoveGoalForm(request.POST or None, instance = obj)
    #if form.is_valid():
     #   obj = form.save(commit= False)
      #  obj.save()
       # return HttpResponseRedirect('/gbolahanscrumy/home' )
    #context = {'move_goal': form }
    #return render(request, 'gbolahanscrumy/movegoal.html', context)

def sign_up(request, *args, **kwargs):
    if request.method =='POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            new_user = form.save()
            new_user.groups.add(Group.objects.get(name='Developer'))
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('/gbolahanscrumy/successpage')
        else:
            return HttpResponse("Invalid credentials provided: Username or Email already exist")
    else:
        form = SignUpForm()
    return render (request, 'gbolahanscrumy/signup.html', {'form' : form})


def add_goal(request, *args, **kwargs):
    current_user = request.user
    usr_grp = request.user.groups.all()[0]
    dev_grp = Group.objects.get(name='Developer')
    develop = User.objects.filter(groups__name__in=['Developer'])
    qa = User.objects.filter(groups__name__in=['Quality Assurance'])
    admin = User.objects.filter(groups__name__in=['Admin'])
    own = User.objects.filter(groups__name__in=['Owner'])
    if request.user in develop:
        form = DeveloperCreateGoalForm()
        dictionary = {'form' : form}
        if request.method =='POST':
            form = DeveloperCreateGoalForm(request.POST)
            if form.is_valid():
                add_goal = ScrumyGoals()
                add_goal = form.save(commit = False)
                add_goal.goal_id = random.randint(1000, 9999)
                add_goal.goal_status = GoalStatus.objects.get(status_name="Weekly Goal")
                add_goal.save()
                return redirect  ("/gbolahanscrumy/home")       
            return HttpResponse("Invalid credentials provided, please fill out all fields")
        else:
            form = DeveloperCreateGoalForm()
            return render (request, 'gbolahanscrumy/addgoal.html', dictionary)
    


    elif request.user in qa:
        form = QACreateGoalForm(request.POST)
        dictionary = {'form' : form}
        if request.method =='POST':
            form = QACreateGoalForm(request.POST)
            if form.is_valid():
                add_goal = ScrumyGoals()
                add_goal = form.save(commit = False)
                add_goal.goal_id = random.randint(1000, 9999)
                add_goal.goal_status = GoalStatus.objects.get(status_name="Weekly Goal")
                add_goal.save()
                
                return redirect  ("/gbolahanscrumy/home")       
            return HttpResponse("Invalid credentials provided, please fill out all fields")
        else:
            form = QACreateGoalForm()
            return render (request, 'gbolahanscrumy/addgoal.html', dictionary)
    elif request.user in admin:
        form = CreateGoalForm()
        dictionary = {'form' : form}
        if request.method =='POST':
            form = CreateGoalForm(request.POST)
            if form.is_valid():
                add_goal = ScrumyGoals()
                add_goal = form.save(commit = False)
                add_goal.goal_id = random.randint(1000, 9999)
                add_goal.save()
                return redirect  ("/gbolahanscrumy/home")       
            return HttpResponse("Invalid credentials provided, please fill out all fields")
        else:
            form = CreateGoalForm()
    
        return render (request, 'gbolahanscrumy/addgoal.html', dictionary)
    elif request.user in own:
        form = CreateGoalForm()
        dictionary = {'form' : form}
        if request.method =='POST':
            form = CreateGoalForm(request.POST)
            if form.is_valid():
                add_goal = ScrumyGoals()
                add_goal = form.save(commit = False)
                add_goal.goal_id = random.randint(1000, 9999)
                add_goal.save()
                return redirect  ("/gbolahanscrumy/home")       
            return HttpResponse("Invalid credentials provided, please fill out all fields")
        else:
            form = CreateGoalForm()
    
        return render (request, 'gbolahanscrumy/addgoal.html', dictionary)

#def add_goal(request):
 #   form = CreateGoalForm
  #  if request.method == 'POST':
   #     form = form(request.POST)
    #    data = request.POST.dict()
     #   user = User.objects.get(id=data['user'])
      #  add_goal = form.save(commit=False)
       # add_goal.goal_id = random.randint(1000,9990)
        #add_goal.created_by = user.username
        #add_goal.moved_by = user.username
        #add_goal.owner = user.username
       # status = GoalStatus.objects.get(id=data['goal_status'])
        #add_goal.goal_status = status
        #add_goal.save()
       # return HttpResponseRedirect('/gbolahanscrumy/home' )
    #context = {'create_goal': form }
    
    #return render(request, 'gbolahanscrumy/addgoal.html', context)





