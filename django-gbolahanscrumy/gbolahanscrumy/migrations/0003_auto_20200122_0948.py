# Generated by Django 3.0.2 on 2020-01-22 09:48

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('gbolahanscrumy', '0002_auto_20200122_0947'),
    ]

    operations = [
        migrations.AlterField(
            model_name='scrumygoals',
            name='goal_status',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, related_name='User', to='gbolahanscrumy.GoalStatus'),
        ),
    ]
